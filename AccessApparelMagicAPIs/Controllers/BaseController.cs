﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AccessApparelMagicAPIs.Controllers
{
    public class BaseController : Controller
    {
        // GET: Base
      

        protected string baseURL = System.Configuration.ConfigurationManager.AppSettings["BaseUrl"];
       

        public async Task<string> CallGetAPI(string url)
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(baseURL);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
                //Sending request to find web api REST service resource url using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(url);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    return Res.Content.ReadAsStringAsync().Result;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

     

        public async Task<HttpResponseMessage> CallPostAPI(string url, object jsonData)
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(baseURL);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource url using HttpClient  
                return await client.PostAsJsonAsync(url, jsonData);
            }
        }

        public async Task<HttpResponseMessage> CallPutAPI(string url, object jsonData)
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(baseURL);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource url using HttpClient  
                return await client.PutAsJsonAsync(url, jsonData);
            }
        }

        public async Task<HttpResponseMessage> CallDeleteAPI(string url)
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(baseURL);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource url using HttpClient  
                return await client.DeleteAsync(url);
            }
        }
    }
}