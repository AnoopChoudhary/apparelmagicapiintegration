﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AccessApparelMagicAPIs.Models;


namespace AccessApparelMagicAPIs.Controllers
{
    public class HomeController : BaseController
    {
        protected string token = System.Configuration.ConfigurationManager.AppSettings["Token"];

        private string GetURL(string functionalityName)
        {
            StringBuilder stbURL = new StringBuilder();
            stbURL.Append(functionalityName);
            stbURL.Append("?token=");
            stbURL.Append(token);
            stbURL.Append("&time=");
            Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            stbURL.Append(unixTimestamp);
            Parameter[] collParameter=new Parameter[1];
            Parameter parameter = new Parameter();
            parameter.field = "last_modified_time";
            parameter.Operator = ">=";
            parameter.value = "2018-04-27";
            parameter.include_type = "OR";
            collParameter[0] = parameter;
            FilterURL filterURL = new FilterURL();
            filterURL.time = unixTimestamp.ToString();
            filterURL.token = token;
            filterURL.parameters = collParameter;
            stbURL.Append("&");
            string builtParameters = QueryStringBuilder.BuildQueryString(
                new Dictionary<string, string>()
                    {
                      { "field", "last_modified_time" },
                      { "operator", ">=" },
                      { "value", "2018-04-27" },
                      { "include_type", "OR" }
                    });
            //string builtParameters = QueryStringBuilder.BuildQueryString(filterURL, "&");
            stbURL.Append(builtParameters);
            return stbURL.ToString();
        }

        public async Task<ActionResult> Index()
        {
            string builtURL = GetURL("customers");
            string jsonResult =await CallGetAPI(builtURL);
  //      string a=    QueryStringBuilder.BuildQueryString(new
  //          {
  //              Age = 19,
  //              Name = "John&Doe",
  //              Values = new object[]
  //                {
  //                  1,
  //                  2,
  //                  new Dictionary<string, string>()
  //                  {
  //                    { "key1", "value1" },
  //                    { "key2", "value2" },
  //                  }
  //                },
  //          });

            //          Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

            //          string c = QueryStringBuilder.BuildQueryString(
            //              new Dictionary<string, string>()
            //                  {

            //                    { "field", "last_modified_time" },
            //                    { "operator", ">=" },
            //                    { "value", "2018-04-27" },
            //                    { "include_type", "OR" }

            //                  }, "¶");


            //          string b = QueryStringBuilder.BuildQueryString(new object[]
            //                     {
            //                       1,
            //                       2,
            //                       new object[] { "one", "two", "three" },
            //                       new Dictionary<string, string>()
            //                       {
            //                         { "key1", "value1" },
            //                         { "key2", "value2" },
            //                       }
            //                     }
            //);

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}