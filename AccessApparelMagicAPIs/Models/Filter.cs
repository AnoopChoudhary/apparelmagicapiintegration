﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccessApparelMagicAPIs.Models
{
  public class FilterURL
    {
        public string token { get; set; }
        public string time { get; set; }
        public Parameter[] parameters { get; set; }
    }

    public class Parameter
    {
        public string field { get; set; }
        public string Operator { get; set; }
        public string value { get; set; }
        public string include_type { get; set; }
    }

}